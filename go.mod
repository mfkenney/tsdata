module bitbucket.org/mfkenney/tsdata

go 1.13

require (
	github.com/influxdata/influxdb1-client v0.0.0-20190809212627-fc22c7df067e
	github.com/pkg/errors v0.8.1
	github.com/urfave/cli v1.22.5
)
