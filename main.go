// Tsdata downloads from an InfluxDB time-series database to a CSV file.
package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

const TIME_FORMAT = "2006-01-02 15:04:05.000000"

var Version = "dev"
var BuildDate = "unknown"

// ParseDatetime is a flexible ISO8601-ish date/time parser. It handles
// the following formats (missing least-significant values are assumed
// to be zero):
//
//    YYYY-mm-ddTHH:MM:SS-ZZZZ
//    YYYY-mm-ddTHH:MM:SS   (UTC assumed)
//    YYYY-mm-ddTHH:MM
//    YYYY-mm-dd
//
func parseDatetime(dt string) (time.Time, error) {
	t, err := time.Parse("2006-01-02T15:04:05-0700", dt)
	if err != nil {
		t, err = time.Parse("2006-01-02T15:04:05", dt)
		if err != nil {
			t, err = time.Parse("2006-01-02T15:04", dt)
			if err != nil {
				t, err = time.Parse("2006-01-02", dt)
			}
		}
	}

	return t, err
}

// Construct a QueryConfig from the command-line arguments
func queryFromContext(c *cli.Context, fields map[string][]string) (QueryConfig, error) {
	var (
		cfg QueryConfig
		err error
	)

	if c.String("tstart") != "" {
		cfg.Tstart, err = parseDatetime(c.String("tstart"))
		if err != nil {
			return cfg, cli.NewExitError(
				errors.Wrap(err, "parse start time"), 1)
		}
	}

	if c.String("tend") != "" {
		cfg.Tend, err = parseDatetime(c.String("tend"))
		if err != nil {
			return cfg, cli.NewExitError(
				errors.Wrap(err, "parse end time"), 1)
		}
	}

	cfg.Tags = make(map[string]string)
	if c.String("tags") != "" {
		for _, s := range strings.Split(c.String("tags"), ",") {
			entry := strings.SplitN(s, "=", 2)
			if len(entry) == 2 {
				cfg.Tags[entry[0]] = entry[1]
			}
		}
	}

	drop := make(map[string]int)
	if c.String("drop") != "" {
		for _, s := range strings.Split(c.String("drop"), ",") {
			drop[s] = 1
		}
	}

	cfg.Dbname = c.Args().Get(0)
	cfg.Measurement = c.Args().Get(1)
	_, ok := fields[cfg.Measurement]
	if !ok {
		return cfg, cli.NewExitError("Unknown measurement", 1)
	}

	// Check for dropped fields (columns)
	cfg.Fields = make([]string, 0)
	for _, name := range fields[cfg.Measurement] {
		if _, found := drop[name]; !found {
			cfg.Fields = append(cfg.Fields, name)
		}
	}

	return cfg, nil
}

func main() {
	app := cli.NewApp()
	app.Name = "tsdata"
	app.Usage = "download data from an InfluxDB time-series database"
	app.Version = Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "server, s",
			Usage:  "Server URL",
			Value:  "http://localhost:8086",
			EnvVar: "TSDB_URL",
		},
		cli.StringFlag{
			Name:   "creds, c",
			Usage:  "DB credentials, `USER:PASSWORD`",
			Value:  "",
			EnvVar: "TSDB_CREDENTIALS",
		},
	}

	var cln client.Client
	var fields map[string][]string

	openDb := func(c *cli.Context) error {
		if c.NArg() < 1 {
			return cli.NewExitError("missing database name", 1)
		}
		dbname := c.Args().Get(0)

		var err error
		cfg := client.HTTPConfig{
			Addr: c.GlobalString("server"),
		}

		if c.GlobalString("creds") != "" {
			if strings.HasPrefix(c.GlobalString("creds"), "@") {
				contents, err := ioutil.ReadFile(c.GlobalString("creds")[1:])
				if err != nil {
					return cli.NewExitError("cannot read credentials", 1)
				}
				parts := strings.SplitN(
					strings.TrimRight(string(contents), "\r\n"), ":", 2)
				cfg.Username = parts[0]
				cfg.Password = parts[1]
			} else {
				parts := strings.SplitN(c.GlobalString("creds"), ":", 2)
				cfg.Username = parts[0]
				cfg.Password = parts[1]
			}
		}

		cln, err = client.NewHTTPClient(cfg)
		if err != nil {
			return cli.NewExitError(err, 1)
		}

		fields, err = getFields(cln, dbname)
		if err != nil {
			return cli.NewExitError(err, 1)
		}

		return nil
	}

	closeDb := func(c *cli.Context) error {
		cln.Close()
		return nil
	}

	app.Commands = []cli.Command{
		{
			Name:            "list",
			Usage:           "list available measurements or fields",
			SkipFlagParsing: true,
			ArgsUsage:       "dbname [measurement]",
			Before:          openDb,
			After:           closeDb,
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					for k, _ := range fields {
						fmt.Fprintf(c.App.Writer, "%s\n", k)
					}
				} else {
					measurement := c.Args().Get(1)
					if cols, found := fields[measurement]; found {
						for _, col := range cols {
							fmt.Fprintf(c.App.Writer, "%s\n", col)
						}
					}
				}

				return nil
			},
		},
		{
			Name:      "get",
			Usage:     "download a measurement in CSV format",
			ArgsUsage: "dbname measurement",
			Before:    openDb,
			After:     closeDb,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "output, o",
					Usage: "write CSV output to `FILE`",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tstart",
					Usage: "start time, YYYY-mm-dd[THH:MM[:SS]]",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tend",
					Usage: "end time, YYYY-mm-dd[THH:MM[:SS]]",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tags",
					Usage: "tags to add to the query, `TAG=VALUE,[TAG=VALUE...]`",
					Value: "",
				},
				cli.StringFlag{
					Name:  "drop",
					Usage: "fields to drop from the measurement, `NAME,[NAME,...]`",
					Value: "",
				},
				cli.BoolTFlag{
					Name:  "header",
					Usage: "include CSV header",
				},
			},
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return cli.NewExitError("missing arguments", 1)
				}

				cfg, err := queryFromContext(c, fields)
				if err != nil {
					return err
				}

				var f io.Writer
				if c.String("output") != "" {
					f, err = os.Create(c.String("output"))
					if err != nil {
						return cli.NewExitError(
							errors.Wrap(err, "open failed"), 1)
					}
				} else {
					f = os.Stdout
				}

				err = writeCsv(cln, f, cfg, c.BoolT("header"))
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				return nil
			},
		},
		{
			Name:      "getbin",
			Usage:     "download a measurement in raw (binary) format",
			ArgsUsage: "dbname measurement",
			Before:    openDb,
			After:     closeDb,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "outdir, o",
					Usage: "write binary files to `DIRECTORY`",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tstart",
					Usage: "start time, YYYY-mm-dd[THH:MM[:SS]]",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tend",
					Usage: "end time, YYYY-mm-dd[THH:MM[:SS]]",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tags",
					Usage: "tags to add to the query, `TAG=VALUE,[TAG=VALUE...]`",
					Value: "",
				},
				cli.BoolTFlag{
					Name:  "base64",
					Usage: "data is encoded as base64 (default is hex)",
				},
			},
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return cli.NewExitError("missing arguments", 1)
				}

				cfg, err := queryFromContext(c, fields)
				if err != nil {
					return err
				}
				cfg.Fields = append(cfg.Fields, "content")

				var topdir string
				if c.String("outdir") != "" {
					topdir = c.String("outdir")
				} else {
					topdir = fmt.Sprintf("%s.d", c.Args().Get(1))
				}

				err = os.MkdirAll(topdir, 0755)
				if err != nil {
					return cli.NewExitError(err, 1)
				}

				err = writeBin(cln, topdir, cfg, c.BoolT("base64"))
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				return nil
			},
		},
		{
			Name:      "gettext",
			Usage:     "download a measurement in raw (text) format",
			ArgsUsage: "dbname measurement",
			Before:    openDb,
			After:     closeDb,
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "output, o",
					Usage: "write output to `FILE`",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tstart",
					Usage: "start time, YYYY-mm-dd[THH:MM[:SS]]",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tend",
					Usage: "end time, YYYY-mm-dd[THH:MM[:SS]]",
					Value: "",
				},
				cli.StringFlag{
					Name:  "tags",
					Usage: "tags to add to the query, `TAG=VALUE,[TAG=VALUE...]`",
					Value: "",
				},
			},
			Action: func(c *cli.Context) error {
				if c.NArg() < 2 {
					return cli.NewExitError("missing arguments", 1)
				}

				cfg, err := queryFromContext(c, fields)
				if err != nil {
					return err
				}
				cfg.Fields = append(cfg.Fields, "content")

				var f io.Writer
				if c.String("output") != "" {
					f, err = os.Create(c.String("output"))
					if err != nil {
						return cli.NewExitError(
							errors.Wrap(err, "open failed"), 1)
					}
				} else {
					f = os.Stdout
				}

				err = writeText(cln, f, cfg)
				if err != nil {
					return cli.NewExitError(err, 1)
				}
				return nil
			},
		},
	}

	app.Run(os.Args)
}
