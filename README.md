# InfluxDB Data Fetcher

Command-line application to download a data-set from an [InfluxDB](https://www.influxdata.com/products/influxdb/) time-series database.

## Installation

Download the latest release for your OS from the [Downloads](https://bitbucket.org/mfkenney/tsdata/downloads/) section of this repository. Unpack the ZIP or compressed TAR archive and copy the executable to some directory on your PATH.

## Usage

There are three subcommands, *list*, *get*, and *getbin*:

``` shellsession
$ tsdata help
NAME:
   tsdata - download data from an InfluxDB time-series database

USAGE:
   tsdata [global options] command [command options] [arguments...]

VERSION:
   dev

COMMANDS:
     list     list available measurements
     get      download a measurement in CSV format
     getbin   download a measurement in raw (binary) format
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --server value, -s value  Server URL (default: "http://10.95.97.213:8086")
   --creds USER:PASSWORD, -c USER:PASSWORD  DB credentials, USER:PASSWORD [$DB_CREDENTIALS]
   --help, -h                show help
   --version, -v             print the version

$ tsdata help list
NAME:
   tsdata list - list available measurements

USAGE:
   tsdata list dbname

$ tsdata help get
NAME:
   tsdata get - download a measurement in CSV format

USAGE:
   tsdata get [command options] dbname measurement

OPTIONS:
   --output FILE, -o FILE           write CSV output to FILE
   --tstart value                   start time, YYYY-mm-dd[THH:MM[:SS]]
   --tend value                     end time, YYYY-mm-dd[THH:MM[:SS]]
   --tags TAG=VALUE,[TAG=VALUE...]  tags to add to the query, TAG=VALUE,[TAG=VALUE...]
   --drop NAME,[NAME...]            field names to drop from the measurement
   --header                         include CSV header

$ tsdata help getbin
NAME:
   tsdata getbin - download a measurement in raw (binary) format

USAGE:
   tsdata getbin [command options] dbname measurement

OPTIONS:
   --outdir DIRECTORY, -o DIRECTORY  write binary files to DIRECTORY
   --tstart value                    start time, YYYY-mm-dd[THH:MM[:SS]]
   --tend value                      end time, YYYY-mm-dd[THH:MM[:SS]]
   --tags TAG=VALUE,[TAG=VALUE...]   tags to add to the query, TAG=VALUE,[TAG=VALUE...]
   --base64                          data is encoded as base64 (default is hex)
```
