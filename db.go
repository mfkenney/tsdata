package main

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"path/filepath"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	client "github.com/influxdata/influxdb1-client/v2"
	"github.com/pkg/errors"
)

func getMeasurements(cln client.Client, dbname string) ([]string, error) {
	q := client.NewQuery("show measurements", dbname, "")
	resp, err := cln.Query(q)
	meas := make([]string, 0)
	if err == nil {
		if resp.Error() == nil {
			for _, row := range resp.Results[0].Series[0].Values {
				meas = append(meas, row[0].(string))
			}
		} else {
			return meas, resp.Error()
		}
	} else {
		return meas, err
	}

	return meas, nil
}

// Return a list of field keys for each measurement (table)
func getFields(cln client.Client, dbname string) (map[string][]string, error) {
	fields := make(map[string][]string)
	q := client.NewQuery("show field keys", dbname, "")
	resp, err := cln.Query(q)

	if err != nil {
		return fields, err
	}
	if resp.Error() != nil {
		return fields, resp.Error()
	}

	for _, s := range resp.Results[0].Series {
		f := make([]string, 0)
		for _, row := range s.Values {
			name := row[0].(string)
			start, _ := utf8.DecodeRuneInString(name)
			if unicode.IsLetter(start) {
				f = append(f, row[0].(string))
			}
		}
		fields[s.Name] = f
	}

	return fields, nil
}

type QueryConfig struct {
	Dbname      string
	Measurement string
	Fields      []string
	Tstart      time.Time
	Tend        time.Time
	Tags        map[string]string
}

func buildQuery(cfg QueryConfig) client.Query {
	var fields string
	if cfg.Fields == nil || len(cfg.Fields) == 0 {
		fields = "*"
	} else {
		fields = strings.Join(cfg.Fields, ",")
	}

	q := fmt.Sprintf("select %s from \"%s\"", fields, cfg.Measurement)
	conds := make([]string, 0)

	if !cfg.Tstart.IsZero() {
		conds = append(conds, fmt.Sprintf("time >= %ds", cfg.Tstart.Unix()))
	}

	if !cfg.Tend.IsZero() {
		conds = append(conds, fmt.Sprintf("time <= %ds", cfg.Tend.Unix()))
	}

	for k, v := range cfg.Tags {
		conds = append(conds, fmt.Sprintf("\"%s\" = '%s'", k, v))
	}

	if len(conds) > 0 {
		q = q + " where " + strings.Join(conds, " and ")
	}

	return client.NewQuery(q, cfg.Dbname, "us")
}

func writeCsv(cln client.Client, f io.Writer, cfg QueryConfig, incHeader bool) error {
	q := buildQuery(cfg)

	resp, err := cln.Query(q)
	if err != nil {
		return err
	}
	if resp.Error() != nil {
		return resp.Error()
	}

	if incHeader {
		_, err = fmt.Fprintln(f, "time,"+strings.Join(cfg.Fields, ","))
		if err != nil {
			return errors.Wrap(err, "file write failed")
		}
	}

	if len(resp.Results[0].Series) == 0 {
		return errors.New("No results for this time range")
	}

	for _, row := range resp.Results[0].Series[0].Values {
		t, _ := row[0].(json.Number).Float64()
		n := len(row)
		ts := time.Unix(int64(t)/1000000000, (int64(t)%1000000000)/1000).UTC()
		fmt.Fprintf(f, "%s,", ts.Format(TIME_FORMAT))
		for i := 1; i < n-1; i++ {
			if row[i] == nil {
				_, err = fmt.Fprintf(f, ",")
			} else {
				_, err = fmt.Fprintf(f, "%v,", row[i])
			}
			if err != nil {
				return errors.Wrap(err, "file write failed")
			}
		}

		if row[n-1] == nil {
			_, err = fmt.Fprintf(f, "\n")
		} else {
			_, err = fmt.Fprintf(f, "%v\n", row[n-1])
		}
		if err != nil {
			return errors.Wrap(err, "file write failed")
		}
	}

	return nil
}

// Generate a timestamp-based filename
func genFilename(t time.Time, name string) string {
	s := t.Format("20060102T150405")
	return fmt.Sprintf("%s_%s.bin", name, s)
}

func writeBin(cln client.Client, topdir string, cfg QueryConfig, useBase64 bool) error {
	q := buildQuery(cfg)

	resp, err := cln.Query(q)
	if err != nil {
		return err
	}
	if resp.Error() != nil {
		return resp.Error()
	}

	if len(resp.Results[0].Series) == 0 {
		return errors.New("No results for this time range")
	}

	var data []byte
	for _, row := range resp.Results[0].Series[0].Values {
		t, _ := row[0].(json.Number).Float64()
		ts := time.Unix(int64(t)/1000000000, (int64(t)%1000000000)/1000).UTC()
		fname := genFilename(ts, cfg.Measurement)
		if useBase64 {
			data, err = base64.StdEncoding.DecodeString(row[1].(string))
		} else {
			data, err = hex.DecodeString(row[1].(string))
		}
		if err != nil {
			return errors.Wrapf(err, "timestamp: %s", ts)
		}
		err = ioutil.WriteFile(filepath.Join(topdir, fname), data, 0644)
		if err != nil {
			return errors.Wrapf(err, "filename: %s", fname)
		}
	}
	return nil
}

func writeText(cln client.Client, f io.Writer, cfg QueryConfig) error {
	q := buildQuery(cfg)

	resp, err := cln.Query(q)
	if err != nil {
		return err
	}
	if resp.Error() != nil {
		return resp.Error()
	}

	if len(resp.Results[0].Series) == 0 {
		return errors.New("No results for this time range")
	}

	for _, row := range resp.Results[0].Series[0].Values {
		_, err = fmt.Fprintf(f, "%s\n", row[1].(string))
		if err != nil {
			return errors.Wrap(err, "file write failed")
		}
	}

	return nil
}
